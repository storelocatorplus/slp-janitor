<?php

/**
 * phpUnit Test for SLPlus Janitor
 *
 * @package PHPUnit\StoreLocatorPlus\Janitor
 * @author Lance Cleveland <lance@charlestonsw.com>
 * @copyright 2014 Charleston Software Associates, LLC
 */
class SLPJanitorTest extends PHPUnit_Framework_TestCase {

    /**
     * @var \SLPlus
     */
    public $slplus;

    /**
     * Setup the global pointer to the main SLPlus plugin object.
     */
    function setUp() {
        parent::setUp();
        $this->slplus = $GLOBALS['slplus_plugin'];
    }

    /**
     * Printout before this test suite runs.
     */
    public static function setUpBeforeClass() {
        print "\n".get_class()."\n";
    }

    /**
     * To next line before next test suite runs.
     */
    public static function tearDownAfterClass() {
        print "\n";
    }

    /**
     * Is the slplus variable an SLPlus object?
     */
    function test_slplus_is_set() {
        $this->assertInstanceOf( 'SLPlus', $this->slplus );
    }
}
